
document.addEventListener('deviceready', function () {

    window.plugins.OneSignal
        .startInit("09b83c69-145f-4622-a10c-e61d1b5afc5a")
        .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.None)
        .handleNotificationOpened(mensagemRecebida)
        .handleNotificationReceived(mensagemRecebida)
        .endInit();

}, false);

function mensagemRecebida(jsonData) {

    if (jsonData.notification.payload) {
        var titulo = jsonData.notification.payload.title;
        var descricao = jsonData.notification.payload.body;
    } else {
        var titulo = jsonData.payload.title;
        var descricao = jsonData.payload.body;
    }

    console.log(jsonData)

    document.getElementById('msg').innerHTML = `
        Titulo: ${titulo} <br>
        Descrição: ${descricao} <br>
    `
}

function enviar() {
    window.plugins.OneSignal
        .startInit("09b83c69-145f-4622-a10c-e61d1b5afc5a")
        .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.None)
        .getIds(function (ids) {

            var notificationObj = {
                contents: { en: "message body" },
                include_player_ids: [ids.userId]
            };

            window.plugins.OneSignal.postNotification(notificationObj,

                function (successResponse) {
                    console.log("Sucesso:", successResponse);
                },
                function (failedResponse) {
                    console.log("Falhou: ", failedResponse);
                    alert("Notification Post Failed:\n" + JSON.stringify(failedResponse));
                }
            );
        })
        .endInit();
}